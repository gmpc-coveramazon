/* gmpc-coveramazon (GMPC plugin)
 * Copyright (C) 2006-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <string.h>
#include <config.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libmpd/debug_printf.h>
#include <gmpc/plugin.h>
#include <gmpc/metadata.h>
#include <gmpc/gmpc_easy_download.h>
#define AMAZONKEY "14TC04B24356BPHXW1R2"

static char * host = "http://ecs.amazonaws.%s/onca/xml?Service=AWSECommerceService&Operation=ItemSearch&SearchIndex=Music&ResponseGroup=Images,EditorialReview&AWSAccessKeyId=%s&%s=%s&%s=%s";

gmpcPlugin plugin;

#define ENDPOINTS 6
static char *endpoints[ENDPOINTS][2] = 
{ 
	{"com", 	N_("United States")},
	{"co.uk",	N_("United Kingdom")},
	{"jp",		N_("Japan")},
	{"fr",		N_("France")},
	{"ca", 		N_("Canada")},
	{"de", 		N_("Germany")}
};

static GtkWidget *wp_pref_vbox = NULL;
typedef struct amazon_song_info {
	char *image_big;
	char *image_medium;
	char *image_small;
	char *album_info;

}amazon_song_info;


typedef struct ii {
    int type;
    mpd_Song *song;
    void (*callback)(GList *list, gpointer data);
    gpointer data;
    amazon_song_info *asi;
    GList *uris;
    int fetch_performer;
}ii;

static void __fetch_metadata_amazon(mpd_Song *song, char *sfield, char *nartist, char *stype,char *nalbum,int type,  ii *i);
/**
 * Get Set enabled
 */
static int amazon_get_enabled()
{
	return cfg_get_single_value_as_int_with_default(config, "cover-amazon", "enable", TRUE);
}
static void amazon_set_enabled(int enabled)
{
	cfg_set_single_value_as_int(config, "cover-amazon", "enable", enabled);
}

/**
 * Get priority 
 */
static int fetch_cover_priority(){
	return cfg_get_single_value_as_int_with_default(config, "cover-amazon", "priority", 80);
}
static void fetch_cover_priority_set(int priority)
{
    cfg_set_single_value_as_int(config, "cover-amazon", "priority", priority);
}

static void fetch_metadata(mpd_Song *song,MetaDataType type,void (*callback)(GList *uris, gpointer data), gpointer data)
{
    ii *i= g_malloc0(sizeof(*i));
    i->type = type;
    i->callback = callback;
    i->data = data;
    i->song = song;
    i->uris = NULL;
    i->fetch_performer = 0;
    if(song == NULL)
    {
        i->callback(NULL, i->data);
        g_free(i);
        return;
    }
	if(song->artist == NULL || song->album == NULL)
	{
        i->callback(NULL, i->data);
        g_free(i);
        return;
	}
	if(type != META_ALBUM_ART && type != META_ALBUM_TXT) {
        i->callback(NULL, i->data);
        g_free(i);
		return;
	}
	/* Always fetch it. */

	gchar *artist = gmpc_easy_download_uri_escape(i->song->artist);
	gchar *album =  gmpc_easy_download_uri_escape(i->song->album);

    __fetch_metadata_amazon(song,
                "Title",album,
                "Artist", artist, 
                type,i);

	g_free(artist);
	g_free(album);
}	

static amazon_song_info * amazon_song_info_new()
{
	amazon_song_info *asi = g_malloc(sizeof(amazon_song_info));
	asi->image_big = NULL;
	asi->image_medium = NULL;
	asi->image_small = NULL;
	asi->album_info = NULL;
	return asi;
}
static void amazon_song_info_free(amazon_song_info *asi)
{
    if(asi) return;
	if(asi->image_big != NULL) g_free(asi->image_big);
	if(asi->image_medium != NULL) g_free(asi->image_medium);
	if(asi->image_small != NULL) g_free(asi->image_small);
	if(asi->album_info != NULL) g_free(asi->album_info);
	g_free(asi);
	return;
}

static xmlNodePtr get_first_node_by_name(xmlNodePtr xml, gchar *name) {
	if(xml) {
		xmlNodePtr c = xml->xmlChildrenNode;
		for(;c;c=c->next) {
			if(xmlStrEqual(c->name, (xmlChar *) name))
				return c;
		}
	}
	return NULL;
}

static amazon_song_info *__cover_art_xml_get_image(const char *data,int size)
{
	xmlDocPtr doc = xmlParseMemory(data,size);
	if(doc)
	{
		xmlNodePtr root = xmlDocGetRootElement(doc);
		xmlNodePtr cur = get_first_node_by_name(root, "Items");
		amazon_song_info *asi = NULL; 
		if(cur) {
			cur = get_first_node_by_name(cur, "Item");
			if(cur) {
				xmlNodePtr child = NULL;
				asi = amazon_song_info_new();
				if((child = get_first_node_by_name(cur, "LargeImage"))) {
					xmlChar *temp = xmlNodeGetContent(get_first_node_by_name(child, "URL")); 
					/* copy it, so we can free it, and don't need xmlFree */
					asi->image_big = g_strdup((char *)temp);
					xmlFree(temp);
				}
				if((child = get_first_node_by_name(cur, "MediumImage"))){
					xmlChar *temp = xmlNodeGetContent(get_first_node_by_name(child, "URL"));
					asi->image_medium = g_strdup((char *)temp);
					xmlFree(temp);
				}
				if((child = get_first_node_by_name(cur, "SmallImage"))){
					xmlChar *temp = xmlNodeGetContent(get_first_node_by_name(child, "URL"));
					asi->image_small = g_strdup((char *)temp);
					xmlFree(temp);
				}	

				if((child = get_first_node_by_name(cur, "EditorialReviews"))) { 
					child = get_first_node_by_name(child, "EditorialReview");
					if(child) {
						xmlChar *temp = xmlNodeGetContent(get_first_node_by_name(child, "Content")); /* ugy, lazy */
						asi->album_info = g_strdup((char *)temp);
						xmlFree(temp);
					}
				}
			}
		}
		xmlFreeDoc(doc);
		return asi;
	}
	
	return NULL;
}
static void parse_result(ii *i)
{
    if(i->asi)
    {
        MetaData *mtd = NULL;
        if(i->type&META_ALBUM_ART)
        {
            if(i->asi->image_big)
            {
                mtd = meta_data_new();
                mtd->type = i->type;
                mtd->plugin_name = plugin.name;
                mtd->content_type = META_DATA_CONTENT_URI;
                mtd->content = g_strdup(i->asi->image_big);
                mtd->size = -1;
                (i->uris)= g_list_append((i->uris), mtd);
            }

            if(i->asi->image_medium)
            {
                mtd = meta_data_new();
                mtd->type = i->type;
                mtd->plugin_name = plugin.name;
                mtd->content_type = META_DATA_CONTENT_URI;
                mtd->content = g_strdup(i->asi->image_medium);
                mtd->size = -1;
                (i->uris)= g_list_append((i->uris),mtd);
            }

            if(i->asi->image_small)
            {
                mtd = meta_data_new();
                mtd->type = i->type;
                mtd->plugin_name = plugin.name;
                mtd->content_type = META_DATA_CONTENT_URI;
                mtd->content = g_strdup(i->asi->image_small);
                mtd->size = -1;
                (i->uris)= g_list_append((i->uris),mtd); 
            }

            i->callback(i->uris, i->data);
            amazon_song_info_free(i->asi);
            g_free(i);
            return;
        }
        else if(i->type&META_ALBUM_TXT)
        {
            if(i->asi->album_info)
            {
                mtd = meta_data_new();
                mtd->type = i->type;
                mtd->plugin_name = plugin.name;
                mtd->content_type = META_DATA_CONTENT_HTML;
                mtd->content = g_strdup(i->asi->album_info);
                mtd->size = -1;
                (i->uris)= g_list_append((i->uris),mtd); 
            }
            i->callback(i->uris, i->data);
            amazon_song_info_free(i->asi);
            g_free(i);
            return;
        }
    }
    if(i->fetch_performer && i->song->performer)
    {
        gchar *performer = gmpc_easy_download_uri_escape(i->song->performer);
        gchar *album =  gmpc_easy_download_uri_escape(i->song->album);
        i->fetch_performer = 1;

        __fetch_metadata_amazon(i->song,
                "Title",album,
                "Performer", performer, 
                i->type, i);

        g_free(performer);
        g_free(album);
        return;
    }
    i->callback(i->uris, i->data);
    if(i->asi)amazon_song_info_free(i->asi);
    g_free(i);
}
static void search_downloaded(const GEADAsyncHandler *handle, GEADStatus status, gpointer user_data)
{
    ii *i = (ii *)user_data;
    if(status == GEAD_PROGRESS) return;
    if(status == GEAD_DONE)
    {
        goffset length;
        const char * data = gmpc_easy_handler_get_data(handle,&length);
		i->asi = __cover_art_xml_get_image(data, (int)length);
        parse_result(i);
        return;
    }
    i->callback(NULL, i->data);
    g_free(i);

}


static void __fetch_metadata_amazon(mpd_Song *song, char *sfield, char *nartist, char *stype,char *nalbum,int type,  ii *i)
{
	char furl[1024];
	int endpoint = cfg_get_single_value_as_int_with_default(config, "cover-amazon", "location", 0);
	char *endp = endpoints[endpoint][0];

	debug_printf(DEBUG_INFO, "search-type: %s\n", stype);
	snprintf(furl,1024, host,endp, AMAZONKEY,sfield, nartist, stype, nalbum);
    gmpc_easy_async_downloader(furl, search_downloaded, i);
}


/**
 * Preferences
 */

static void amazon_cover_art_pref_destroy(GtkWidget *container)
{
	gtk_container_remove(GTK_CONTAINER(container), wp_pref_vbox);
}
static void amazon_cover_art_pref_selection_changed(GtkWidget *box)
{
	cfg_set_single_value_as_int(config, "cover-amazon", "location", gtk_combo_box_get_active(GTK_COMBO_BOX(box)));
}

static void amazon_cover_art_pref_construct(GtkWidget *container)
{
	GtkWidget *label = NULL;
	GtkWidget *selection = NULL;
	GtkWidget *hbox = NULL;
	int i;
	wp_pref_vbox = gtk_vbox_new(FALSE,6);


	/* Location */
	hbox = gtk_hbox_new(FALSE, 6);
	label = gtk_label_new(_("Location:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE,0);
	selection = gtk_combo_box_new_text();
	for(i=0;i<ENDPOINTS;i++)
		gtk_combo_box_append_text(GTK_COMBO_BOX(selection), _(endpoints[i][1]));
	gtk_combo_box_set_active(GTK_COMBO_BOX(selection),cfg_get_single_value_as_int_with_default(config, "cover-amazon", "location", 0));
	g_signal_connect(G_OBJECT(selection), "changed",G_CALLBACK(amazon_cover_art_pref_selection_changed), NULL);

	gtk_box_pack_start(GTK_BOX(hbox), selection, TRUE,TRUE,0);
	gtk_box_pack_start(GTK_BOX(wp_pref_vbox), hbox, FALSE, FALSE, 0);

	gtk_container_add(GTK_CONTAINER(container), wp_pref_vbox);
	gtk_widget_show_all(container);
}

gmpcPrefPlugin cam_pref = {
	.construct      = amazon_cover_art_pref_construct,
	.destroy        = amazon_cover_art_pref_destroy

};
gmpcMetaDataPlugin cam_cover = {
	.get_priority   = fetch_cover_priority,
    .set_priority   = fetch_cover_priority_set,
	.get_metadata   = fetch_metadata
};

int plugin_api_version = PLUGIN_API_VERSION;

static void amazon_init(void)
{
	bindtextdomain(GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}
static const gchar *amazon_get_translation_domain(void)
{
    return GETTEXT_PACKAGE;
}
gmpcPlugin plugin = {
	.name           = N_("Amazon Cover Fetcher"),
	.version        = {PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION},
	.plugin_type    = GMPC_PLUGIN_META_DATA,
    .init           = amazon_init,
	.pref           = &cam_pref,
	.metadata       = &cam_cover,
	.get_enabled    = amazon_get_enabled,
	.set_enabled    = amazon_set_enabled,

    .get_translation_domain = amazon_get_translation_domain
};
